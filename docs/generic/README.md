# Common Attributuion and Sources
## Intro / Outro music
- Track Title: [Big Deal](https://soundcloud.com/twisterium/big-deal)
- Music by [Twisterium](https://soundcloud.com/twisterium)
- Music Link: https://soundcloud.com/twisterium/big-deal

---
# Logos
### - [PeerTube](https://commons.wikimedia.org/wiki/File:Logo_de_PeerTube.svg)
English: PeerTube contributorsFrançais : Les contributeurs PeerTube, Public domain, via Wikimedia Commons
### - [Mastodon](https://commons.wikimedia.org/wiki/File:Mastodon_Logotype_(Simple).svg)
Jin Nguyen, [AGPL](http://www.gnu.org/licenses/agpl.html), via Wikimedia Commons
### - [Fediverse](https://commons.wikimedia.org/wiki/File:Fediverse_logo_proposal.svg)
Eukombos, CC0, via Wikimedia Commons
### - [Odysee](https://odysee.com/@OdyseeHelp:b/odyseepresskit:b)

---
# Creative Commons
Unless otherwised mentioned videos are licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) unless otherwise mentioned.
