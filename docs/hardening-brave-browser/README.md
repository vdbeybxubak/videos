# Hardening Brave Browser | Stop Using Google Chrome 
Brave out of the box feels bloated and overwhelming. But has great privacy features turned on by default. 
In this video we will harden Brave, add some extensions, configure some flags & make it look minimal. It offers perfect compatibility with Google Chrome plus by blocking ads it pricks Google's control over the ad-infested web. Although Brave has their controversial crypto 'BAT' it is turned off by default. Brave offers the best compatibility to privacy ratio when compared to the Tor Browser / Hardened Firefox.

# Watch on
- [LBRY / Odysee](https://odysee.com/@polarhive:e/hardening-brave-browser:7)
- [tilvids.com](https://tilvids.com/videos/watch/8a267137-06dd-426c-ac8c-50840709bce9)
- [peertube.social](https://peertube.social/videos/watch/f3195598-99cb-4319-a06a-d94c3502a57e)
 
---
## Articles / Relevant Links
- Get Brave: https://brave.com
- Previous Video: https://polarhive.ml/videos/notes/foss-browsers-for-privacy-desktop/

## BRAVE://FLAGS
- [brave://flags/#isolate-origins](brave://flags/#isolate-origins)
- [brave://flags/#cross-origin-isolated](brave://flags/#cross-origin-isolated)
- [brave://flags/#strict-origin-isolation](brave://flags/#strict-origin-isolation)
- [brave://flags/#freeze-user-agent](brave://flags/#freeze-user-agent)

---
## Attribution
- Track Title: Say Yeah 
- Music by Topher Mohr and Alex Elena
- Music Link: https://redirect.invidious.io/watch?v=-wiUIs9I9EM

---
This video by Nathan Matthew Paul is licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)

For Generic Intro Music and end credit logos attribution visit: [/docs/generic/](https://codeberg.org/polarhive/videos/src/branch/main/docs/generic) 

---
> Patreon, Liberapay, Crypto, Donate and all the other ways you can contribute like video suggestions: https://polarhive.ml/help