## FOSS Browsers for Privacy
Tor browser is the only real browser for 'Privacy'. It ships with strong anti-fingerprinting mitigations by default and masks your IP address. In addition to that - I showcase ungoogled-Chromium, Firefox & Brave. I also explain why you shouldn't use proprietary browsers like Edge, Safari & Google Chrome.

# Watch on
- [LBRY / Odysee](https://odysee.com/@polarhive:e/foss-browsers-for-privacy-desktop:0)
- [tilvids.com](https://tilvids.com/videos/watch/38bb8f48-5e62-4df4-96a4-22cd57a9cac0)
- [peertube.social](https://peertube.social/videos/watch/75475bae-401d-4981-8754-db400fe051c6)
 
---
## Articles / Relevant Links
Related Video: https://polarhive.ml/videos/notes/hardening-brave-browser/

- Brave Crypto Scandal
https://www.theverge.com/2020/6/8/21283769/brave-browser-affiliate-links-crypto-privacy-ceo-apology

- Firefox / Chromium Sandboxing
https://madaidans-insecurities.github.io/firefox-chromium.html

- Mozilla political
https://blog.mozilla.org/blog/2021/01/08/we-need-more-than-deplatforming/

- Ungoogled-Chromium GitHub
https://github.com/Eloston/ungoogled-chromium

- Google Chrome proprietary
https://www.google.com/intl/en/chrome/terms/

- Chrome vs Chromium
https://chromium.googlesource.com/chromium/src/+/master/docs/chromium_browser_vs_google_chrome.md

- Librewolf for GNU/Linux
https://gitlab.com/librewolf-community/browser/linux/-/releases

- Tor 
https://www.torproject.org/download/

- Tor Project FAQ
https://support.torproject.org/faq/

- My Article on Fireofox/Mozilla
https://polarhive.ml/blog/firefox/

- Lunduke's Video on Firefox/Mozilla
https://odysee.com/@Lunduke:e/mozilla-is-not-trustworthy:a

---
## Attribution
- Track Title: Say Yeah 
- Music by Topher Mohr and Alex Elena
- Music Link: https://redirect.invidious.io/watch?v=-wiUIs9I9EM

---
This video by Nathan Matthew Paul is licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)

For Generic Intro Music and end credit logos attribution visit: [/docs/generic/](https://codeberg.org/polarhive/videos/src/branch/main/docs/generic) 

---
> Patreon, Liberapay, Crypto, Donate and all the other ways you can contribute like video suggestions: https://polarhive.ml/help