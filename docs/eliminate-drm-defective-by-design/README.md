# Eliminate DRM - Defective By Design 
DRM - Digital Restrictions Management is a nasty way to get spied on and is a failure by design. Censoring Digital files is like trying to make water not wet. Spotify, Netflix, Amazon, Hulu, Steam, Apple, Google all bundle DRM and harm user freedom. Stop the injustice of DRM - slowly switch to ethical ways to consume media, try one of the alternatives listed below ->

# Watch on
- [LBRY / Odysee](https://odysee.com/@polarhive:e/eliminate-drm-defective-by-design:3)
- [tilvids.com](https://tilvids.com/videos/watch/230e2d50-9ce7-488f-811f-5147f713bd9b/)
- [peertube.social](https://peertube.social/videos/watch/0e8d35bb-b3c0-4a9f-b359-5cd43a4eb179)

# Alternatives
- https://bandcamp.com/fair_trade_music_policy
- https://www.gog.com/
- http://itch.io/
- http://ytdl-org.github.io/youtube-dl/about.html
- https://puri.sm/
- [LBRY/Odysee](https://polarhive.ml/odysee)

## Articles / Relevant Links
- https://www.defectivebydesign.org/
- https://io9.gizmodo.com/amazon-secretly-removes-1984-from-the-kindle-5317703/
- https://www.defectivebydesign.org/faq/
- https://news.sky.com/story/netflix-accused-of-spying-on-users-after-creepy-tweet-11167513/
- https://web.archive.org/web/20180511091418/https://help.netflix.com/legal/eula/
- https://www.ifixit.com/News/9236/day-against-drm/
- https://www.wired.com/2012/10/amazons-remote-wipe-of-customers-kindle-highlights-perils-of-drm/
- https://www.theguardian.com/technology/blog/2014/feb/05/digital-rights-management/
- https://polarhive.ml/blog/apple/
- https://en.wikipedia.org/wiki/Always-on_DRM
- https://redirect.invidious.io/watch?v=Ez3f1HgOa1o 
- https://redirect.invidious.io/watch?v=FY7DtKMBxBw

---
## Attribution
- Track Title: Machina
- Music by [Scott Buckley](https://www.scottbuckley.com.au/)
- Music Link: https://www.scottbuckley.com.au/library/machina/

---
- https://commons.wikimedia.org/wiki/File:BBC_Eliminate_DRM.jpg -
Mattl at English Wikipedia, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons

- https://commons.wikimedia.org/wiki/File:DRM_protest_Boston_DefectiveByDesign.jpg - Karen Rustad from Claremont, CA, USA, CC BY 2.0 <https://creativecommons.org/licenses/by/2.0>, via Wikimedia Commons

---
This video by Nathan Matthew Paul is licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)

For Generic Intro Music and end credit logos attribution visit: [/docs/generic/](https://codeberg.org/polarhive/videos/src/branch/main/docs/generic) 

---
> Patreon, Liberapay, Crypto, Donate and all the other ways you can contribute like video suggestions: https://polarhive.ml/help