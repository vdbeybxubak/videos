# Everyone Should Make Your Own Simple Website | Setup a free static site in 5 minutes
A simple HTML site, free of cost 🌐. No WiX, Squarespace, or any of the 'consumerized' website ecosystems which dilute the freedom & fun the web was built for!

It doesn't matter if you're a 'coder' or 'programmer' Just follow the video, fill in your website's /about page with a bio. Add links to your social accounts in a neat /contact page, experiment 🧪 with the colors and showcase your site to your friends. 

This video isn't meant to 'spoon-feed' you. You are in control of your website, you can learn at your own pace. You can think freely without worrying about BigTech algorithms or censorship of your ideas. Spark ✨ your curiosity & cookup an indie website to take you back to the retro web! Comment below and I'll feature your websites.

# Watch on
- [LBRY / Odysee](https://odysee.com/@polarhive:e/everyone-should-make-your-own-simple-website:e)
- [tilvids.com](https://tilvids.com/videos/watch/e9567603-9395-447d-8050-4f0be77d5b6c)
- [peertube.social](https://peertube.social/videos/watch/93cc621b-5e22-4bbc-bdfd-14b8b046265a)

# What do I fork?
https://codeberg.org/polarhive/simplewebsite/

## Articles / Relevant Links
- https://codeberg.org/polarhive/simplewebsite/
- https://indieweb.org/why
- https://neocities.org/
- https://tildeverse.org/
- https://developer.mozilla.org/en-US/docs/Learn 
- https://github.com/LukasJoswiak/etch
- https://gohugo.io/
- https://polarhive.ml/

## Featured websties
- https://ozaki-yutaka.codeberg.page/
- https://mogli.codeberg.page/
- < your website here 👀 >

---
## Attribution
- Track Title: Say Yeah 
- Music by Topher Mohr and Alex Elena
- Music Link: https://redirect.invidious.io/watch?v=-wiUIs9I9EM

- Track Title: Road Tripzzz
- Music by [Ofshane](https://redirect.invidious.io/channel/UC34Wh4ysdP50H-ThbZFFfsA)
- Music Link: https://redirect.invidious.io/watch?v=BRglIP5jrGo

---
- Upstream repo for simplewebsite
- https://github.com/LukasJoswiak/etch/blob/master/LICENSE

---
This video by Nathan Matthew Paul is licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)

For Generic Intro Music and end credit logos attribution visit: [/docs/generic/](https://codeberg.org/polarhive/videos/src/branch/main/docs/generic) 

---
> Patreon, Liberapay, Crypto, Donate and all the other ways you can contribute like video suggestions: https://polarhive.ml/help