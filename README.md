# A repository for the description/sources/attributions for my videos
I'm [Nathan](https://polarhive.ml). I make videos and talk about tech, privacy, tutorials on free & open source software on this channel.

This channel is a portal/hub of all the videos I film & edit. I had 14k views on YouTube, now all those videos are archived, after deleting my main Google account. I'm starting fresh on decentralized platforms like [Odysee](https://polarhive.ml/odysee) and [PeerTube](https://polarhive.ml/peertube). Follow me on [Mastodon](https://polarhive.ml/mastodon) and [Pixelfed](https://polarhive.ml/pixelfed).

---
Patreon, Liberapay, Crypto, Donate and all the other ways you can contribute: 
https://polarhive.ml/help

## Where do you upload your videos?
- [LBRY / Odysee](https://polarhive.ml/odysee) 
- [PeerTube [tilvids.com] (ActivityPub)](https://polarhive.ml/peertube)
- [PeerTube [peertube.social] (ActivityPub)](https://peertube.social/accounts/nathan/video-channels)
- [I also upload on YouTube but don't want to bring people into centralized silos](https://polarhive.ml/blog/fedi-first)

## Why not write it in the description box itself?
1. I want to bring people away from YouTube and into privacy friendly, decentralized alternatives like the fediverse, LBRY and eventually self host my own PeerTube. Both protocols allow for decentralized hosting.
2. It's easier to manage it in one place like this repo than updating one video at a time.
3. YouTube tracks outbound links in the desciption.
4. Helps me learn Git

---
## #FediFirst - Upload on Fedi/LBRY a week before you post to YouTube
### https://polarhive.ml/blog/fedi-first

---
## Intro/Outro music and other Generic files.. 
[/docs/generic](https://codeberg.org/polarhive/videos/src/branch/main/docs/generic/README.md)
